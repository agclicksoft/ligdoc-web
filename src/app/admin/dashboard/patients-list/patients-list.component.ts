import { Component, OnInit } from '@angular/core';
import { Patients } from 'src/app/classes/patients';
import { PatientsService } from 'src/app/services/patients.service';
import { NgxSpinnerService } from 'ngx-spinner';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-patients-list',
  templateUrl: './patients-list.component.html',
  styleUrls: ['./patients-list.component.css']
})
export class PatientsListComponent implements OnInit {

  patients: Patients = new Patients();
  keyword: string;
  totalPatients: number;
  p: number = 1;

  constructor(
    private patientsService: PatientsService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit(): void {
    this.getAllPatients(this.p);
  }

  getAllPatients(p) {
    this.spinner.show();
    return this.patientsService.getAllPatients(p).subscribe(
      res => {
        this.patients = res,
        this.totalPatients = this.patients.total,
        this.spinner.hide()
      },
      err => {
        console.log(err)
      }
    )
  }

  findPatients(keyword) {
    this.keyword = keyword;
    this.patientsService.findPatients(this.p, this.keyword).subscribe(
      res => {
        this.patients = res
        if (this.p > this.patients.lastPage) {
          this.p = 1;
          this.findPatients(this.keyword);
        }
      },
      err => {
        console.log(err)
      }
    )
  }

  pageChanged(p) {
    this.p = p;
    this.totalPatients === this.patients.total ? this.getAllPatients(this.p)
      : this.findPatients(this.keyword)

  }

}
