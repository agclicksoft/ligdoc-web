import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  onIndex: boolean;

  constructor(
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.menuToggle();
  }

  menuToggle() {
    if (window.innerWidth < 768) {
      const menu = document.querySelector('ul');

      if (menu.getAttribute('style') === 'top: -100vh;')
        menu.setAttribute('style', 'top: 0');
      
      else
        menu.setAttribute('style', 'top: -100vh;')
    }
  }

  logout() {
    this.authService.logout('admin');
  }
  
}
