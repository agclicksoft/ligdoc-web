import { Component, OnInit } from '@angular/core';

import { DoctorsService } from 'src/app/services/doctors.service';
import { Doctors } from 'src/app/classes/doctors';
import { PatientsService } from 'src/app/services/patients.service';
import { Patients } from 'src/app/classes/patients';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.css']
})
export class PanelComponent implements OnInit {

  access: string = 'Ativo';

  doctors: Doctors = new Doctors();
  countOf: any = { doctors: null, patients: null };
  patients: Patients = new Patients();
  patientsDoctor: number;

  constructor(
    private commonService: CommonService,
    private doctorsService: DoctorsService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit(): void {
    this.getCountOfUsers();
    this.getAllDoctors();
  }

  getCountOfUsers() {
    this.commonService.getCountOfUsers().subscribe(
      res => {this.countOf = res},
      err => {console.log(err)}
    )
  }

  getAllDoctors() {
    this.spinner.show();
    this.doctorsService.getAllDoctors(1).subscribe(
      res => { this.doctors = res, this.spinner.hide() },
      err => { console.log(err), this.spinner.hide() }
    )
  }

}


