import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormArray, FormGroup } from '@angular/forms';
import { ViacepService } from 'src/app/services/viacep.service';
import { DoctorsService } from 'src/app/services/doctors.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register-doctors',
  templateUrl: './register-doctors.component.html',
  styleUrls: ['./register-doctors.component.css']
})
export class RegisterDoctorsComponent implements OnInit {

  status: boolean = false;
  differentPasswords: boolean = true;
  isSubmitted: boolean = false;

  doctorsForm = this.fb.group({
    crm: [''],
    rqe: [''],
    cnpj: [''],
    status: [''],
    user: this.fb.group({
      password: [''],
      confirmPassword: [''],
      name: [''],
      cpf: [''],
      birth_day: [''],
      genre: [''],
      email: [''],
      cellphone: [''],
      contact: [''],
      address_zipcode: [''],
      address_street: [''],
      address_number: [''],
      address_complement: [''],
      address_neighborhood: [''],
      address_city: [''],
      address_uf: [''],
      accounts: this.fb.array([
        this.fb.array([
          this.fb.control(''),
          this.fb.control(''),
          this.fb.control('')
        ])
      ]),
    }),
    academicsEducation: this.fb.group({
      institute_location: [''],
      institute_conclusion: [''],
      residence_location: [''],
      residence_conclusion: [''],
      formation_include: [''],
    }),    
    experiences: this.fb.array([
      this.fb.array([
        this.fb.control(''),
        this.fb.control(''),
        this.fb.control('')
      ])
    ])
    // specialties: [''],
  })

  constructor(
    private fb: FormBuilder,
    private viaCep: ViacepService,
    private doctorsService: DoctorsService
  ) { }


  ngOnInit(): void {
    this.doctorsForm.patchValue({ status: false })
  }

  get experiences() {
    return this.doctorsForm.get('experiences') as FormArray;
  }

  get accounts() {
    return this.user.get('accounts') as FormArray;
  }

  get user() {
    return this.doctorsForm.get('user') as FormGroup;
  }

  get academicsEducation() {
    return this.doctorsForm.get('academicsEducation') as FormGroup;
  }

  addExperiences() {
    this.experiences.push(this.fb.array([
      this.fb.control(''),
      this.fb.control(''),
      this.fb.control(''),
    ]));
  }

  addAccounts() {
    this.accounts.push(this.fb.array([
      this.fb.control(''),
      this.fb.control(''),
      this.fb.control(''),
    ]))
  }

  // FUNÇÃO PARA FAZER FAZER UPLOAD DE ARQUIVOS AO CLICAR NA IMAGEM
  uploadImage() {
    document.getElementById('upload-image').click();
  }

  // FUNÇÃO PARA BOTÃO TOGGLE DE ACESSO AO APLICATIVO
  toggleAccess() {
    if (this.status) {
      this.status = false;
      this.doctorsForm.patchValue({ status: false })
      document.getElementById('btn-toggle').classList.remove('on');
      document.getElementById('btn-toggle-inner').removeAttribute('style');
    }
    else {
      this.status = true;
      this.doctorsForm.patchValue({ status: true })
      document.getElementById('btn-toggle').classList.add('on');
      document.getElementById('btn-toggle-inner').setAttribute('style', 'left: 40px;' )
    }
  }

  confirmPassword() {
    if (this.user.value.confirmPassword == this.user.value.password || 
        this.user.value.confirmPassword == '' || this.user.value.password == '') {
      this.differentPasswords = false;
    }
    else {
      this.differentPasswords = true;
    }
  }

  // FUNÇÃO PARA FAZER O UPLOAD DE DOCUMENTOS AO CLICAR NO BOTÃO
  uploadDocs() {
    document.getElementById('upload-docs').click();
  }

  getAddress() {
    if (this.doctorsForm.value.user.address_zipcode.length === 8)
      this.viaCep.getAddress(this.doctorsForm.value.user.address_zipcode).subscribe(
        res => { this.setAddress(res) },
        err => { console.log(err) }
      ) 
  }

  setAddress(data) {
    console.log(data)
    this.user.patchValue({
      address_street: data.logradouro,
      address_neighborhood: data.bairro,
      address_city: data.localidade,
      address_uf: data.uf
    })
  }

  onSubmit() {

    this.isSubmitted = true;

    console.log(this.doctorsForm)

    if (this.doctorsForm.status === 'INVALID')
      return

    if (this.differentPasswords)
      return Swal.fire('Erro', 'As senhas precisam ser iguais', 'error');


    alert('ok');

    const experiences = this.experiences.value;
    for (let i in experiences) 
      this.experiences.value[i] = {
        location: experiences[i][0],
        start: experiences[i][1],
        end: experiences[i][2]
      }

    const accounts = this.accounts.value;
    for (let i in accounts)
      this.accounts.value[i] = {
        institution: accounts[i][0],
        agency: accounts[i][1],
        account: accounts[i][2]
      }

      const academicsEducation = this.doctorsForm.value.academicsEducation
      this.doctorsForm.value.academicsEducation = [academicsEducation]

    
    // this.doctorsService.setDoctors(this.doctorsForm.value).subscribe(
    //   res => { console.log(res) },
    //   err => { console.log(err) }
    // )    

    console.log(this.doctorsForm)
  }
}
