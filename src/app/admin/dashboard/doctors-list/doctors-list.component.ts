import { Component, OnInit } from '@angular/core';
import { DoctorsService } from 'src/app/services/doctors.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Doctors } from 'src/app/classes/doctors';

@Component({
  selector: 'app-doctors-list',
  templateUrl: './doctors-list.component.html',
  styleUrls: ['./doctors-list.component.css']
})
export class DoctorsListComponent implements OnInit {

  doctors: Doctors = new Doctors();
  totalDoctors: number;
  keyword: string;
  p: number = 1;

  constructor(
    private doctorsService: DoctorsService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit(): void {
    this.getAllDoctors(this.p);
  }

  getAllDoctors(p) {
    this.spinner.show();
    this.doctorsService.getAllDoctors(p).subscribe(
      res => {
        this.doctors = res,
        this.totalDoctors = this.doctors.total,
        this.spinner.hide()
      },
      err => {
        console.log(err)
      }
    )
  }

  findDoctors(keyword) {
    this.keyword = keyword;
    this.doctorsService.findDoctors(this.p, this.keyword).subscribe(
      res => {
        this.doctors = res
        if (this.p > this.doctors.lastPage) {
          this.p = 1;
          this.findDoctors(this.keyword);
        }
      },
      err => {
        console.log(err)
      }
    )
  }

  pageChanged(p) {
    this.p = p;
    this.totalDoctors === this.doctors.total ? this.getAllDoctors(this.p)
      : this.findDoctors(this.keyword)
  }

}
