import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  unauthorized: boolean = false;
  anotherError: boolean = false;

  constructor(
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.authService.loginError.subscribe(
      loginError => { 
        if (loginError.status === 401) this.unauthorized = true
        else this.anotherError = true
      }
    )
  }

  onSubmit(f) {
    if (f.valid) {
      console.log(f)
      f.submitted = false;
      this.authService.login(f.value)
    }
  }

  teste(f) {
    console.log(f)
  }

  ngOnDestroy() {
    this.authService.loginError.unsubscribe();
  }

}
