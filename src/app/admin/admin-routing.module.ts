import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AdminAuthGuard } from '../auth/admin-auth.guard';
import { DoctorsListComponent } from './dashboard/doctors-list/doctors-list.component';
import { PartnersListComponent } from './dashboard/partners-list/partners-list.component';
import { PatientsListComponent } from './dashboard/patients-list/patients-list.component';
import { ReportsListComponent } from './dashboard/reports-list/reports-list.component';
import { PanelComponent } from './dashboard/panel/panel.component';
import { RegisterDoctorsComponent } from './dashboard/register-doctors/register-doctors.component';


const routes: Routes = [
  { path: 'admin/login', component: LoginComponent },
  { path: 'admin/dashboard', component: DashboardComponent, canActivate: [AdminAuthGuard], children: [
    { path: '', pathMatch: 'full', redirectTo: 'painel' },
    { path: 'painel', component: PanelComponent },
    { path: 'medicos', component: DoctorsListComponent },
    { path: 'medicos/cadastrar', component: RegisterDoctorsComponent },
    { path: 'pacientes', component: PatientsListComponent },
    { path: 'parceiros', component: PartnersListComponent },
    { path: 'relatorios', component: ReportsListComponent }
  ]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
