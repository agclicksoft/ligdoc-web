import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { AdminRoutingModule } from "./admin-routing.module";
import { LoginComponent } from "./login/login.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DoctorsListComponent } from "./dashboard/doctors-list/doctors-list.component";
import { PatientsListComponent } from "./dashboard/patients-list/patients-list.component";
import { PartnersListComponent } from "./dashboard/partners-list/partners-list.component";
import { ReportsListComponent } from "./dashboard/reports-list/reports-list.component";
import { PanelComponent } from "./dashboard/panel/panel.component";
import { FooterComponent } from '../shared/footer/footer.component';
import { SharedModule } from '../shared/shared.module';
import { UtilitiesModule } from '../utilities/utilities.module';
import { RegisterDoctorsComponent } from './dashboard/register-doctors/register-doctors.component';

@NgModule({
  declarations: [
    LoginComponent,
    DashboardComponent,
    DoctorsListComponent,
    PatientsListComponent,
    PartnersListComponent,
    ReportsListComponent,
    PanelComponent,
    RegisterDoctorsComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AdminRoutingModule,
    UtilitiesModule,
    SharedModule
  ],
})
export class AdminModule {}
