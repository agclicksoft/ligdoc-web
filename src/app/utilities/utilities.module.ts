import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxSpinnerModule } from 'ngx-spinner';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxMaskModule } from 'ngx-mask'



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    NgxSpinnerModule,
    NgxPaginationModule,
    NgxMaskModule.forRoot()
  ],
  providers: [
  ],
  exports: [
    NgxSpinnerModule,
    NgxPaginationModule,
    NgxMaskModule
  ]
})
export class UtilitiesModule { }
