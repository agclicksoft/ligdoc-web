import { Users } from './users';

export class Doctors {
    total: number;
    perPage: number;
    page: number;
    lastPage: number;
    data: Array<Data> = [new Data()];
}

class Data {
    id: number;
    user_id: number;
    crm: string;
    rqe: string;
    status: boolean;
    created_at: Date;
    updated_at: Date;
    cnpj: string;
    user: Users = new Users();
    specialties: Specialties = new Specialties();
}

class Specialties {
    id: number;
    description: string;
    created_at: Date;
    updated_at: Date;
    pivot: Pivot = new Pivot();
}

class Pivot {
    specialtie_id: number;
    doctor_id: number;
}