import { Users } from './users';

export class Patients {
    total: number;
    perPage: number;
    page: number;
    lastPage: number;
    data: Array<Data> = [new Data()];
}

class Data {
    id: number;
    weight: string
    height: string;
    created_at: Date;
    updated_at: Date;
    user_id: number;
    question1: string;
    question2: string;
    question3: string;
    user: Users = new Users();
}
