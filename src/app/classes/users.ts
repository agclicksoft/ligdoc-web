export class Users {
    id: number;
    email: string;
    password: string;
    name: string;
    role: string;
    img_profile: string;
    contact: string;
    cpf: string;
    genre: string;
    birth_day: Date;
    address_street: string;
    address_number: string;
    address_complement: string;
    address_city: string;
    address_uf: string;
    address_neighborhood: string;
    tk_payment: string;
    accept_terms: boolean;
    created_at: Date;
    updated_at: Date;
    address_zipcode: string;
    tk_firebase: string;
    cellphone: string
}
