import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { environment } from 'src/environments/environment';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const token = localStorage.getItem('token') || sessionStorage.getItem('token');

    if (request.url.indexOf(environment.API) != -1 && token) {

      const cloned = request.clone({ headers: request.headers.set('Authorization', 'Bearer ' + token) });

      const expiresAt = moment().add(15, 'minutes');

      sessionStorage.setItem('expires_at', JSON.stringify(expiresAt.valueOf()));

      return next.handle(cloned);
    }
    else {

      return next.handle(request);
    }
  }
}
