import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AdminAuthGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private router: Router
  ) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

      const hasToken = sessionStorage.getItem('token') || localStorage.getItem('token');
      const hasRtkn = sessionStorage.getItem('rtkn') || localStorage.getItem('rtkn')
      const isLoggedIn = this.authService.isLoggedIn();
    
      if ( hasToken && hasRtkn && isLoggedIn )
        return true;

      return false;
  }
  
}
