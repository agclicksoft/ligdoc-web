import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';

import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import * as moment from 'moment';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  loginError = new EventEmitter<any>();

  constructor(
    private http: HttpClient,
    private router: Router,
    private spinner: NgxSpinnerService,
  ) { }

  public login(data) {
    this.spinner.show();
    this.http.post<any>(`${environment.API}auth/login`, data).subscribe(
      res => { this.setSession(res, data), console.log(res) },
      err => { this.spinner.hide(), this.loginError.emit(err) },
      ()  => { this.router.navigate(['/admin/dashboard']) }
    )
  }

  private setSession(res, data) {
    if (data.remember)
      return this.setLocal(res);
    sessionStorage.setItem('token', res.data.token);
    sessionStorage.setItem('rtkn', res.data.refreshToken);
    sessionStorage.setItem('id', res.user.id);
    sessionStorage.setItem('expiresAt', JSON.stringify( moment().add(15, 'minutes').valueOf() ))
  }

  private setLocal(res) {
    localStorage.setItem('token', res.data.token);
    sessionStorage.setItem('rtkn', res.data.refreshToken);
    localStorage.setItem('id', res.user.id);
    localStorage.setItem('expiresAt', JSON.stringify( moment().add(7, 'days').valueOf() ))
  }

  public logout(path) {
    localStorage.clear();
    sessionStorage.clear();
    this.router.navigate([`/${path}/login`])
  }

  public isLoggedIn() {
    return moment().isBefore(this.getExpiration());
  }

  public getExpiration() {
    const expiration = localStorage.getItem('expiresAt') || sessionStorage.getItem('expiresAt');
    const expiresAt = JSON.parse(expiration);
    return moment(expiresAt);
  }


}
