import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Doctors } from '../classes/doctors';

@Injectable({
  providedIn: 'root'
})
export class DoctorsService {

  constructor(
    private http: HttpClient
  ) { }

  getAllDoctors(p) {
    return this.http.get<Doctors>(`${environment.API}doctors?page=${p}`);
  }

  setDoctors(data) {
    return this.http.post<Doctors>(`${environment.API}doctors`, data);
  }

  findDoctors(p, keyword) {
    return this.http.get<Doctors>(`${environment.API}doctors?page=${p}&search=${keyword}`);
  }

}
