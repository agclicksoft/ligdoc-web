import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';

import { Patients } from '../classes/patients';

@Injectable({
  providedIn: 'root'
})
export class PatientsService {

  constructor(
    private http: HttpClient
  ) { }

  getAllPatients(p) {
    return this.http.get<Patients>(`${environment.API}patients?page=${p}`);
  }

  findPatients(p, keyword) {
    return this.http.get<Patients>(`${environment.API}patients?page=${p}&search=${keyword}`)
  }
}
